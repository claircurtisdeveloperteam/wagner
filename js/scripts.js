//
//
// Title: Wagner teszt oldal
// Version: 1.0
// Author: Clair and Curtis HU
//
//

$(document).ready(function(){

	$('.icon-envelope').on('click', function() {
		$('.envelope-open').toggleClass('icon-opened');
		$('.search-open').removeClass('icon-opened');
	});

	$('.icon-search').on('click', function() {
		$('.search-open').toggleClass('icon-opened');
		$('.envelope-open').removeClass('icon-opened');
	});

});